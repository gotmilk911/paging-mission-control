#This program was developted to Ingest status telemetry data and create alert messages.
#Version 1.0
#Created JB on Feb 10th 2020
 
#!/bin/bash
 
txtred='\e[0;31m' #red
txtgrn='\e[0;32m' #green
txtrst='\e[0m'  #reset
 
usage(){
 cat << EOF
You need to enter the file that you'd like the program to look at
-f, file location and name of the file
 
SAMPLE COMMAND: ./mission_control.sh -f ./rawfile.txt
 
EOF
}
 
while getopts :f:h OPTIONS; do
 case ${OPTIONS} in
   h)
     usage; exit 0
     ;;
   f)
     FILELOCATION="$OPTARG"
     ;;
   :)
     echo "Invalid option-$OPTARG requires information" 1>2
     usage
     exit 1
     ;;
   ?)
     echo "ERROR: unknown option $(1)."
     usage
     exit 1
     ;;
 esac
done
 
if [[ -z "$FILELOCATION" ]]; then
 usage
 exit 1
fi
 
rawData=( $(sed 's/ /|/g' $FILELOCATION ) )
 
## to call function: printOutput satellite1000batt
function printOutput() {
 counter=0
  
 ## assign array name to arrayName to avoid confusion in awk statements
 arrayName=("$@")
 
 timestamp=$(echo $(date -d ${arrayName[0]:0:8} +%F)T${arrayName[0]:9:12}Z )
#echo "array contains: ${arrayName[@]}"
 
 component=$(echo ${arrayName[0]} | awk -F'|' '{print $NF}' )
 if [ $component == "BATT" ]; then
   severity=LOW
 else
   severity=HIGH
 fi
 
   ## prints first 21 characters, replaces pipe for space to be accepted by the date command, and converts to seconds for easier comparison:
 startTime=$( echo ${arrayName[0]:0:21} | sed 's/|/ /' | date  -d - +%s%N )
 endTime=$( echo $startTime | date -d +5min +%s%N )
 #echo ${satellite1000batt[0]} |awk -F'|' '{print $1" "$2}'
  ## have to go through array to see what is within 5 minutes:
 #for item in ${#arrayName[@]}; do
 for item in ${arrayName[@]}; do
   #echo "loop is inspecting: $item"
   itemDate=$(echo $item| cut -c 21 - | sed 's/|/ /' | date  -d - +%s%N )
   #itemDate=$( echo ${arrayName[$item]:0:21} | sed 's/|/ /' | date  -d - +%s%N )
 
   if [ $itemDate -le $endTime ]; then
      counter=$((counter+1))
   fi
 done
 
 # if we have three events within 5 minutes, then process first item in the array...extracting fields to satisfy printout.
 if [ $counter -ge 3 ]; then
   satelliteId=$(echo ${arrayName[0]} | awk -F'|' '{print $3}' )
   timestamp=$(echo $(date -d ${arrayName[0]:0:8} +%F)T${arrayName[0]:9:12}Z )
 
   printf "
{
   \"${txtred}satelliteId${txtrst}\": ${txtgrn}$satelliteId${txtrst},
   \"${txtred}severity${txtrst}\": \"${txtred}RED $severity${txtrst}\",
   \"${txtred}component${txtrst}\": \"${txtred}$component${txtrst}\",
   \"${txtred}timestamp${txtrst}\": \"${txtred}$timestamp${txtrst}\"
},"
 
 fi
}
 
for item in ${rawData[@]}; do
 satellite1000batt+=( $( echo $item | awk -F'|' '$3 == "1000" { if($9 == "BATT") print}' ) )
 satellite1000tstat+=( $( echo $item | awk -F'|' '$3 == "1000" { if($9 == "TSTAT") print}' ) )
 satellite1001batt+=( $( echo $item | awk -F'|' '$3 == "1001" { if($9 == "BATT") print}' ) )
 satellite1001tstat+=( $( echo $item | awk -F'|' '$3 == "1001" { if($9 == "TSTAT") print}' ) )
done
 ## call the printouput function to process this array.
 ## if no errors, then save memory by releasing variable
 printOutput "${satellite1000batt[@]}" && unset satellite1000batt
 printOutput "${satellite1000tstat[@]}" && unset satellite1000tstat
 printOutput "${satellite1001batt[@]}" && unset satellite1001batt
 printOutput "${satellite1001tstat[@]}" && unset satellite1001tstat

